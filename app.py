import typing as t
import joblib
import pandas as pd

from pathlib import Path
from flask import Blueprint, Flask, current_app, jsonify, request


def load_model() -> t.Any:
    filepath = Path(".") / "model.joblib"
    return joblib.load(filepath)


def predict_model(model: t.Any, housing_type: str, surface: int, rooms: int) -> int:
    housing_type_map = {
        "house": "Maison",
        "appartment": "Appartement",
    }
    params = pd.DataFrame({
        "type_local": [housing_type_map[housing_type]],
        "surface_reelle_bati": [surface],
        "nombre_pieces_principales": [rooms],
    })
    predictions =  model.predict(params)
    return int(predictions[0][0])


home = Blueprint("home", __name__)


@home.route("/", methods=["GET", "POST"])
def index() -> str:
    if request.method == "POST":
        housing_type = request.json.get("housing_type")
        surface = request.json.get("surface")
        rooms = request.json.get("rooms")
        prediction = predict_model(
            current_app.config["model"],
            housing_type,
            surface,
            rooms,
        )
        return jsonify(dict(prediction=prediction))

    return jsonify(
        dict(
            name="predictimmo",
            inputs=["housing_type", "surface", "rooms"],
            outputs=["prediction"],
        ),
    )


def create_app() -> Flask:
    app = Flask(__name__)
    app.config["model"] = load_model()
    app.register_blueprint(home, url_prefix="")
    return app
