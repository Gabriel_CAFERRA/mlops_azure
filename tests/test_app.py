from http import HTTPStatus
from flask import url_for
from flask.testing import FlaskClient


def test_get_index_success(client: FlaskClient) -> None:
    response = client.get("/")
    assert response.status_code == HTTPStatus.OK
    assert response.mimetype == "application/json"

    data = response.json
    assert data["name"] == "predictimmo"
    assert data["inputs"] == ["housing_type", "surface", "rooms"]
    assert data["outputs"] == ["prediction"]


def test_post_index_success(client: FlaskClient) -> None:
    params = dict(
        housing_type="appartment",
        surface="50",
        rooms="3",
    )

    response = client.post("/", json=params)
    assert response.status_code == HTTPStatus.OK
    assert response.mimetype == "application/json"

    data = response.json
    assert data["prediction"] > 0.0
